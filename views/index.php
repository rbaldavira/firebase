<div class="container">
    <div class="text-center">

<div class="hide mt-5">
    <h2>Dados do aluno</h2><br>


    <div class="col-lg-12" style="margin: 0 auto; width: 50%">
    <table>
        <tr>
            <td><img src="https://suap.ifsp.edu.br/media/alunos/150x200/42924.jpg" /></td>
            <td>&nbsp;&nbsp;&nbsp;</td>
            <td>

            <h4>Raquel Benetti Baldavira</h4>
            <h5>RA: GU166105-1</h5>
            <h5>E-mail: bbaldavira@gmail.com</h5>
            <h5>Noturno - 3o Semestre</h5>

            </td>
        </tr>
    </table>
                
    </div>

    


    <br />
    <hr>
    <br />
    <a href="#" class="btn btn-elegant"><i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>&nbsp;&nbsp;Dados do Aluno</a>
    <a href="#" class="btn btn-elegant">Sobre a API Firebase&nbsp;&nbsp;<i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
</div>

<div class="welcome show mt-5">
    <div>
        <img class="circle" id="avatar" style="width: 150px; height: 150px;">
        <h4 class="mt-2" id="name"></h4>
        <h5 id="email"></h5>
    </div>
    <div style="margin-top: 50px">
        <button class="btn btn-unique" onclick="logOut()"><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Sair</button>
    </div>
</div>
    </div>
</div>